import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http'
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {ListTodos} from '../app/Models/TodoList.model';
import { HttpClientModule } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TodosService {
  private extractData(res: Response) {

    if (res.status < 200 || res.status >= 300) {
        throw new Error('This request has failed ' + res.status);
    }
    // If everything went fine, return the response
    else {
        return res.json();

    }

}
uri ='http://localhost:3000/api/todos'
  constructor(private http:HttpClient) { }

getTodos()
{
  
  return this.http.get<ListTodos>('http://localhost:3000/api/todos/')
  // .map(this.extractData)
  
  // })

    
}
getTodosById(id)
{
  return this.http.get('http://localhost:3000/api/todos/${id}');
}

CreateTodos(name,completed,description)
{
  const list={
    name:name,
    completed:completed,
    description:description
  }
  return this.http.post(`${this.uri}/`,list);
}


UpdateTodos(id,name,completed,description)
{
  const list={
    
    name:name,
    completed:completed,
    description:description
  }
  return this.http.post(`${this.uri}/${id}`,list);
}

}
