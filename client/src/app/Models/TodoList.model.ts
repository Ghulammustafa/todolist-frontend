export interface ListTodos {
    _id:string;
    name:  string;
    completed:  boolean;
    description: string;
    // constructor(_id:'1',
    //     name:  "String",
    //     completed:  "Boolean",
    //     description: "String"){}
}